const { get } = require('axios')
const debug = require('debug')('rhea:hub:user')
const getAccessToken = require('./access-token')
const { baseEndpoint: { api }, defaultApiConfiguration } = require('../../config/hub')

let user = {}

async function getUsername () {
  debug('get username')
  if (!user.username) {
    debug('no user found. will fetch it')
    user = await getUserInfos()
  }
  return user.username
}

async function getUserInfos () {
  let accessToken = ''
  debug('user', `api endpoint is ${api}`)
  try {
    accessToken = await getAccessToken()
  } catch (e) {
    throw e
  }
  return get('/v1/users?me=true', Object.assign({}, defaultApiConfiguration, {
    baseURL: api,
    headers: {
      'Authorization': `Bearer ${accessToken}`
    }
  }))
    .then(({ data: { data: [ userInformations ] } }) => {
      debug('user ok', user)
      user = userInformations
      return userInformations
    })
    .catch(({ response, request, message }) => {
      debug('user ko', response, message)
      if (response) {
        return Promise.reject(new Error(`User informations retrieval failed.\nServer replied with a ${response.status} status and body: ${JSON.stringify(response.data)}`))
      } else {
        return Promise.reject(new Error(`User info retrieval errored: ${message}`))
      }
    })
}

exports.getUsername = getUsername
