const { pack } = require('tar-pack')
const debug = require('debug')('rhea:hub:pack')

async function getPackageStream (packageDir) {
  debug('pack start', packageDir)
  return new Promise((resolve, reject) => {
    const buffers = []
    pack(packageDir, {
      ignoreFiles: ['.gitignore', '.libignore'],
      filter: ({ path, basename, dirname }) => {
        const includeEntry = !dirname.includes('/node_modules') &&
          !dirname.includes('/.git') &&
          basename !== '.stdlib' &&
          basename !== '.DS_Store'
        debug(path, basename, dirname, includeEntry)
        return includeEntry
      }
    })
      .on('data', buffer => buffers.push(buffer))
      .on('end', () => {
        debug('end!', packageDir)
        resolve(Buffer.concat(buffers))
      })
  })
}

module.exports = getPackageStream
