const { post } = require('axios')
const debug = require('debug')('rhea:hub:accesstoken')
const { baseEndpoint: { api }, defaultApiConfiguration, email, password: defaultPassword } = require('../../config/hub')
let accessToken = ''

async function getAccessToken () {
  debug('get access token')
  if (!accessToken) {
    debug('will fetch new access token')
    accessToken = await logIn()
  }
  debug('done, return')
  return accessToken
}

function logIn (username = email, password = defaultPassword) {
  debug('login')
  return post('/v1/access_tokens', {
    grant_type: 'password',
    username,
    password
  }, Object.assign({}, defaultApiConfiguration, {
    baseURL: api
  }))
    .then(({ data: { data: [ { access_token: apiAccessToken } ] } }) => {
      debug('login ok', apiAccessToken)
      accessToken = apiAccessToken
      return apiAccessToken
    })
    .catch(({ response, request, message }) => {
      debug('login ko', response, message)
      if (response) {
        return Promise.reject(new Error(`Login failed.\nServer replied with a ${response.status} status and body: ${JSON.stringify(response.data)}`))
      } else {
        return Promise.reject(new Error(`Login errored: ${message}`))
      }
    })
}

module.exports = getAccessToken
