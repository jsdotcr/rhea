const { post } = require('axios')
const debug = require('debug')('rhea:hub:up')
const { join } = require('path')
const { isRelease, getLibEnvironment, baseEndpoint: { registry } } = require('../../config/hub')
const getAccessToken = require('./access-token')
const getPackageStream = require('./pack')
const { getUsername } = require('./user')

async function up ({ serviceName, env }) {
  let accessToken = ''
  let username = ''
  debug('up')
  try {
    accessToken = await getAccessToken()
    debug('access token fetched, going with username now')
    username = await getUsername()
    debug('username fetched', username)
  } catch (e) {
    throw e
  }
  const packagePath = join(process.cwd(), username, serviceName)
  const { stdlib: { name, build }, version } = require(join(packagePath, 'package.json'))
  const endpoint = isRelease() ? `${name}@${version}` : `${name}@${getLibEnvironment(env)}`
  debug(serviceName, packagePath, isRelease(), endpoint, build)

  return post(endpoint, await getPackageStream(packagePath), {
    baseURL: registry,
    timeout: 30000,
    headers: {
      'Authorization': `Bearer ${accessToken}`,
      'X-Stdlib-Build': build || ''
    },
    onUploadProgress: function (progressEvent) {
      debug('in progress')
      if (progressEvent.lengthComputable) {
        var percentComplete = progressEvent.loaded / progressEvent.total
        debug('progress', percentComplete)
        debug('progress', progressEvent.loaded, progressEvent.total)
      }
    }
  })
    .then(response => {
      debug('upload done')
      console.log('ok', response.status, response.data)
    })
    .catch(reason => {
      debug('fail')
      debug('response', reason.response)
      debug('message', reason.message)
      debug('config', reason.config)
      process.exit(-1)
    })
}

module.exports = up
