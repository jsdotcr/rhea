const debug = require('debug')('rhea:hub:listservices')
const { accessSync, constants: { R_OK }, readdir } = require('fs')
const { join, resolve } = require('path')
const { promisify } = require('util')
const { servicesToPublish } = require('../../config/hub')
const { getUsername } = require('./user')

async function listServices (forEachCallback = () => {}) {
  const servicesDir = resolve(process.cwd(), await getUsername())
  const services = await promisify(readdir)(servicesDir)
  return services.reduce((serviceListAccumulator, serviceName, i) => {
    const servicePath = join(servicesDir, serviceName)
    const servicePackageJson = join(servicePath, 'package.json')

    if (servicesToPublish && !servicesToPublish.includes(serviceName)) {
      debug(`skipping ${serviceName}. It is not whitelisted`)
      return serviceListAccumulator
    }

    try {
      accessSync(servicePackageJson, R_OK)
      serviceListAccumulator.push({
        serviceName,
        servicePath,
        packageJson: require(servicePackageJson)
      })
    } catch (e) {
      console.error('err!', e)
    }

    return serviceListAccumulator
  }, [])
}

module.exports = listServices
