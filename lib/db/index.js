require('dotenv').config({
  path: '../../.env'
})
const mongoose = require('mongoose')
mongoose.Promise = global.Promise

const {
  DB_USER,
  DB_PASSWORD,
  DB_HOST,
  DB_REPLICA_SET
} = process.env
const dbName = 'hestiadb'

module.exports = async function connection () {
  return mongoose.connect(`mongodb://${DB_USER}:${DB_PASSWORD}@${DB_HOST}/${dbName}?ssl=true&replicaSet=${DB_REPLICA_SET}&authSource=admin`)
}
