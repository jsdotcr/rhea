require('dotenv').config({
  path: '../../.env'
})
const chalk = require('chalk')
const debug = require('debug')
const lib = require('lib')
const { token } = require('../config/hub')

const prefix = 'rhea'

class Logger {
  constructor ({
    enableDebug = process.env.NODE_ENV === 'development',
    name = 'global'
  }) {
    if (name.includes(prefix)) {
      throw new Error(`Please do not include ${prefix} in the service name!`)
    }
    this.debug = enableDebug ? debug(`rhea:${name}`) : function () {}
    if (token) {
      this.lib = lib({
        token
      })
      this.utils = lib.utils({
        service: name
      })
    }
  }
  log (message = '') {
    this.debug(message)
    this.utils && this.utils.log(message)
  }
  warn (message = '') {
    this.debug(chalk.orange(message))
    this.utils && this.utils.warn(message)
  }
  error (message = '') {
    this.debug(chalk.red(message))
    this.utils && this.utils.error(message)
  }
}

module.exports = Logger
