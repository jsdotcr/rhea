const Mongoose = require('mongoose')
const Joigoose = require('joigoose')(Mongoose)

exports.toMongooseModel = function convert (modelName, joiSchema) {
  const joigooseSchema = Joigoose.convert(joiSchema)
  return Mongoose.model(modelName, joigooseSchema)
}
