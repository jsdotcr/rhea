const Joi = require('joi')
const { toMongooseModel } = require('../../lib/schemas/convert')

const ModelName = 'Meal'
const MealSchema = Joi.object().keys({
  recipe: Joi.string().meta({ type: 'ObjectId', ref: 'Recipe' }),
  users: Joi.array().items(Joi.string().meta({ type: 'ObjectId', ref: 'User' })),
  date: Joi.date(),
  status: Joi.string().valid(['planned', 'active', 'done']),
  price: Joi.number(),
  note: Joi.string()
})
const MealMongooseModel = toMongooseModel(ModelName, MealSchema)

exports.JoiSchema = MealSchema
exports.MongooseModel = MealMongooseModel
