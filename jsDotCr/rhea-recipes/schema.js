const Joi = require('joi')
const { toMongooseModel } = require('../../lib/schemas/convert')

const ModelName = 'Recipe'
const RecipeSchema = Joi.object().keys({
  ingredients: Joi.array().items(
    Joi.string().meta({ type: 'ObjectId', ref: 'Ingredient' })
  ),
  notes: Joi.string().options(),
  rating: Joi.array().items(
    Joi.number()
  )
})
const RecipeMongooseModel = toMongooseModel(ModelName, RecipeSchema)

exports.JoiSchema = RecipeSchema
exports.MongooseModel = RecipeMongooseModel
