const Joi = require('joi')
const { toMongooseModel } = require('../../lib/schemas/convert')

const ModelName = 'Charge'
const ChargeSchema = Joi.object().keys({
  from: Joi.string(Joi.string().meta({ type: 'ObjectId', ref: 'User' })),
  to: Joi.string().meta({ type: 'ObjectId', ref: 'User' }),
  price: Joi.number(),
  meal: Joi.string().meta({ type: 'ObjectId', ref: 'Meal' }),
  paid: Joi.boolean().default(false)
})

const ChargeMongooseModel = toMongooseModel(ModelName, ChargeSchema)

exports.JoiSchema = ChargeSchema
exports.MongooseModel = ChargeMongooseModel
