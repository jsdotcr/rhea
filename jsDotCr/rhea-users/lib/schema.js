const Joi = require('joi')
const { toMongooseModel } = require('../../lib/schemas/convert')

const ModelName = 'User'
const UserSchema = Joi.object().keys({
  name: Joi.string().required(),
  token: Joi.uuid(), // ??
  role: Joi.string().valid(['user', 'demi_chef', 'sous_chef', 'chef'])
})

const UserMongooseModel = toMongooseModel(ModelName, UserSchema)

exports.JoiSchema = UserSchema
exports.MongooseModel = UserMongooseModel
