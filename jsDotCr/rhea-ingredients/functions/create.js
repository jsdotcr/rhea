const DB = require('../../../lib/db')
const Logger = require('../../../lib/logger')
const { MongooseModel: Ingredient } = require('../lib/schema')

const logger = new Logger({
  name: 'ingredients:create'
})

/**
* Creates a new ingredient
* @param {string} name Ingredient name
* @param {array} types Ingredient tags
* @returns {object}
*/
module.exports = async (name = 'pasta', types = [], context) => {
  logger.log(name)
  logger.log(types)
  await DB()
  const ingredient = new Ingredient({ name, types })
  logger.log(ingredient)

  return ingredient.save()
    .then((ingredient) => {
      return ingredient
    })
    .catch((error) => {
      logger.error(error)
      return error
    })
}
