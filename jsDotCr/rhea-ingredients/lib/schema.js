const Joi = require('joi')
const { toMongooseModel } = require('../../../lib/schemas/convert')

const ModelName = 'Ingredient'
const IngredientJoiSchema = Joi.object().keys({
  name: Joi.string().required(),
  types: Joi.array().items(
    Joi.string().valid(['vegetables', 'oil', 'meat', 'fish', 'dairy', 'eggs', 'cereals (pasta, corn, wheat, rice, ...)', 'legumes (beans, peas, lentils, et cetera)', 'nuts', 'herbs and spices', 'fruit', 'soy'])
  )
})
const IngredientMongooseModel = toMongooseModel(ModelName, IngredientJoiSchema)

exports.JoiSchema = IngredientJoiSchema
exports.MongooseModel = IngredientMongooseModel
