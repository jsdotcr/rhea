const { bold, cyan } = require('chalk')
const listServices = require('../../lib/hub/list-services')
const up = require('../../lib/hub/up')

exports.command = 'up <env>'
exports.describe = 'Release/updates the microservices'
exports.builder = function (yargs) {
  return yargs
    .describe('env', 'current git branch name/slug')
}

exports.handler = async function ({ env }) {
  console.log(`Will ${cyan('deploy')}…`)

  const servicesList = await listServices()
  servicesList.forEach(({ serviceName, servicePath }) => {
    console.log(`… ${bold(serviceName)}`)
    // spawn('lib', ['up', stdlibEnvironment], spawnOptions(servicePath))
    up({ serviceName, env })
  })
}
