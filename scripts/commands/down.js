const { bold, red } = require('chalk')
const { spawn } = require('child_process')
const { getLibEnvironment, spawnOptions, isFeature } = require('../../config/hub')
// const { handler: loginHandler } = require('./login')
const listServices = require('../../lib/hub/list-services')

exports.command = 'down <env>'
exports.describe = 'Terminates the microservice environment'
exports.builder = function (yargs) {
  return yargs
    .describe('env', 'current git branch name/slug')
}

exports.handler = async function terminate ({ env }) {
  // await loginHandler({ email, password })
  const stdlibEnvironment = getLibEnvironment(env)
  if (!isFeature(env)) {
    throw new Error('What are you trying to do? Terminate command is only allowed for feature branches')
  }
  console.log(`Will ${bold(red('tear down'))}…`)

  const servicesList = await listServices()
  servicesList.forEach(({ serviceName, servicePath, packageJson: { version } }) => {
    console.log(`… ${bold(serviceName)}`)
    spawn('lib', ['down', stdlibEnvironment], spawnOptions(servicePath))
  })
}
