require('dotenv').config()
const debug = require('debug')('rhea:hub:config')
const argv = require('yargs').argv

const RELEASE_BRANCH = 'master'
const LIB_RELEASE_ENV = '--release'
const DEV_BRANCH = 'develop'
const LIB_DEV_ENV = 'dev'

const servicesToPublish = ['rhea-charges']

const baseEndpoint = {
  api: process.env['STDLIB_ENDPOINT_API'] || 'https://api.polybit.com',
  registry: process.env['STDLIB_ENDPOINT_REGISTRY'] || 'https://registry.stdlib.com'
}
const defaultApiConfiguration = {
  timeout: 30000,
  headers: {
    'Content-Type': 'application/json'
  }
}
const token = process.env['STDLIB_ACCESS_TOKEN']
const email = process.env['STDLIB_EMAIL']
const password = process.env['STDLIB_PASSWORD']

function getLibEnvironment (branchName = argv.env) {
  debug('getenv', argv)
  if (!branchName) {
    throw new Error('there is no environment defined!')
  }
  if (isRelease()) {
    debug('release')
    return LIB_RELEASE_ENV
  } else if (isDev()) {
    debug('dev')
    return LIB_DEV_ENV
  } else {
    debug('branch')
    return branchName
  }
}

function isRelease (branchName = argv.env) {
  return branchName === RELEASE_BRANCH
}
function isDev (branchName = argv.env) {
  return branchName === DEV_BRANCH
}
function isFeature (branchName = argv.env) {
  return !isRelease() && !isDev()
}

function spawnOptions (modulePath = '.') {
  return {
    env: process.env,
    cwd: modulePath,
    stdio: 'inherit'
  }
}

exports.baseEndpoint = baseEndpoint
exports.defaultApiConfiguration = defaultApiConfiguration
exports.email = email
exports.getLibEnvironment = getLibEnvironment
exports.isFeature = isFeature
exports.isRelease = isRelease
exports.password = password
exports.servicesToPublish = servicesToPublish
exports.spawnOptions = spawnOptions
exports.token = token
